module.exports = {
  building: {
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    BARRACK: {
      motif: ' O ',
      width: 5,
      height: 5
    },
    TOWER: {
      motif: ' T ',
      width: 3,
      height: 3
    },
    GATE_H: {
      model: ["[  ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "   ", "WWW"]
    },
    FARM: {
      motif: '###',
      width: 4,
      height: 4
    },
    STABLE: {
      motif: ' S ',
      width: 5,
      height: 5
    },
    ARCHERY: {
      motif: ' A ',
      width: 5,
      height: 5
    },
    SIEGE_WORKSHOP: {
      motif: ' % ',
      width: 5,
      height: 5
    },
    FORUM: {
      motif: ' R ',
      width: 6,
      height: 6
    },
    MARKET: {
      motif: ' £ ',
      width: 4,
      height: 3
    },
    CHURCH: {
      motif: ' + ',
      width: 5,
      height: 4
    },
    CASTLE: {
      motif: ' C ',
      width: 6,
      height: 6
    },
    UNIVERSITY: {
      motif: ' U ',
      width: 5,
      height: 4
    },
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      }
    }
  },
  ressource: {
    WOOD: {
      motif: '}|{'
    },
    ROCK: {
      motif: ' R '
    }
  },
};
